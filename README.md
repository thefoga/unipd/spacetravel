# Spacetravel - group project for #tecweb course of Università di Padova

[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)


## Install
Copy all the files and insert [space.sql](https://github.com/Giorat/spacetravel/blob/master/php/space.sql) in your personal MySQL server then set the right permission and login in [db.php](https://github.com/Giorat/spacetravel/blob/master/php/mvc_admin/model/db.php) then login in as admin with username: admin and password: admin.
Add one destination of modify one of the current ones and you'll automatically generate the showcase website.

## Questions and issues
The [github issue tracker](https://github.com/sirfoga/tecweb/issues) is **only** for bug reports and feature requests. Anything else, such as questions for help in using the library, should be posted as [pull requests](https://github.com/sirfoga/tecweb/pulls).

## License
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) Version 2.0, January 2004
