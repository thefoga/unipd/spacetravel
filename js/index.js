
/** Controls every page user and admin side to manage javascript elements */
window.addEventListener("load", function() {
    document.getElementById("menu").classList.add("javascript");

    document.getElementById("hamburger").classList.remove("noJavascript");
    document.getElementById("close_btn").classList.remove("noJavascript");

});

/**
 * Trigger menu open or close by adding or removing css classes
 */
function trigger_menu(){
    var menu = document.getElementById("menu");
    var menuHide = document.getElementById("menu_hide");

    if(menu.classList.contains("menuShow")){
        menu.classList.remove("menuShow");
        menuHide.classList.remove("hideContent")
    }else{
        menu.classList.add("menuShow");
        menuHide.classList.add("hideContent")
    }
}//open or close menu
/** END Controls every page user and admin side to manage javascript elements */

//END COMMON PART, now you'll see specific controls and behaviours for specific pages of both sites, user and admin side.

/** Controls page subscribed.html user side */
if(document.getElementById("destination_parent") && document.getElementById("destination_message") && document.getElementById("destination_link")){
    var id = getUrlVars()["id"];
    if(id >= 0){
        document.getElementById("destination_link").innerHTML = "Go back to the destination";
        document.getElementById("destination_link").href  = "dest"+id+".html";
    }
}

/**
 * get Url variables in a similiar way to PHP $_GET
 * @returns associative array, getting variable from it with this synatx
 * var paramUrl = getUrlVars()["NAME_OF_THE_URL_PARAM"];
 */
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function(m,key,value) {
            vars[key] = value;
        });
    return vars;
}
/** END Controls page subscribed.html user side */

/** Controls page edit.php admin side */
if(document.getElementById("editor_form")){
    var editForm = document.getElementById("editor_form");

    if (document.addEventListener) {
        editForm.addEventListener("submit", function(evt) {
            if(!checkFormAdmin()){
                evt.preventDefault();
            }
        }, true);
    }
    else {
        editForm.attachEvent('onsubmit', function(evt){
            if(!checkFormAdmin()){
                evt.preventDefault();
            }
        });
    }
}

/**
 * Check Edit destination form Admin side
 * if the curator didn't set correct values for the fields img url and planet name
 * it will add a warning message to the page
 * @returns 1 if the form fields are set correctly, 0 if it doesn't
 */
function checkFormAdmin(){


    var planetName = document.getElementById("planet_title").value;

    var planetUrl = document.getElementById("planet_imageurl").value;

    if(!isGoodPlanet(planetName) || !isGoodUrl(planetUrl)){
        var formErrors = document.getElementById("form_errors");
        formErrors.innerHTML = '<h3> Warning </h3><p>';
        if(!isGoodPlanet(planetName))
            formErrors.innerHTML += 'Insert planet name without spaces or strange characters.<br/>';
        if(!isGoodUrl(planetUrl))
            formErrors.innerHTML += 'Insert planet image with a remote url.';
        formErrors.innerHTML += '</p>';
    }


    return (isGoodPlanet(planetName) && isGoodUrl(planetUrl));
}
/** END Controls page edit.php admin side */


/** Controls and logic page list*.html user side
 *  Controls and logic search.php/list.php admin side */
if(document.getElementById("nav_pages")){
    var form = document.getElementById("nav_pages");

    var numPages = parseInt(document.getElementById("numPages").innerText);

    //prevent form submit if fields value is not correct
    if (document.addEventListener) {
        form.addEventListener("submit", function(evt) {
            evt.preventDefault();
            var pagina = parseInt(this.elements.page.value);
            //check if page exists
            if(pagina >0 && pagina <= numPages){
                window.location.href = 'list'+pagina+'.html';
            }
        }, true);
    }
    else {
        form.attachEvent('onsubmit', function(evt){
            evt.preventDefault();
            var pagina = this.elements.page.value;
            //check if page exists
            if(pagina >0 && pagina <= numPages){
                window.location.href = 'list'+pagina+'.html';
            }
        });
    }
}
var URL_REGEX = new RegExp('(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})');
var LETTERS_NUMBERS_REGEX = new RegExp('^\\w+$');

function isGoodRegex(candidate, pattern) {
    return pattern.test(candidate);
}

function isGoodPlanet(candidate) {
    return isGoodRegex(candidate, LETTERS_NUMBERS_REGEX);
}

function isGoodUrl(candidate) {
    return isGoodRegex(candidate, URL_REGEX);
}
/** END Controls and logic page list*.html user side
 *  END Controls and logic search.php/list.php admin side */


