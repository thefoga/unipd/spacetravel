<?php

/**
 * Creates raw HTML for user base layout
 */
class userLayoutBase
{

    /**
     * generates HTML code of the pages of the site
     * @param $title title of page
     * @param $position page position
     * @param $page actual page to get menu
     * @param $meta
     * @param $content actual page content
     * @return string complete HTML for webpage
     */
    public static function show($title, $position, $page, $meta,$content)
    {

        $menuH = self::getMenuHtml($page);

        return <<<EOD

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Script-Type" content="text/javascript"/>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <title>{$title} Space Travel</title>
    
    <meta name="author" content="Spacetravel" />
    <meta name="description" content="{$meta}"/>
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Spacetravel" />
    <meta name="twitter:creator" content="@Spacetravel" />
    
    <link href="./css/style.css" rel="stylesheet" />
    <link rel="stylesheet" media="screen and (min-width: 480px)" href="./css/stylemore480.css" />
    <link rel="stylesheet" media="screen and (min-width: 1024px)" href="./css/stylemore1024.css" />
     
    <link rel="icon" type="image/png" href="./img/logo.png"/>
</head>
<body id="body_id">

<div id="skip-link">
    <a href="#content" class="element-invisible">Skip to the main content</a>
    <a href="#menu" class="element-invisible">Go the menu</a>
</div>

<div id="header">
    <img id="logo" alt="" src="./img/logo.png" />
    <h1 id="page_title">SPACE TRAVEL</h1>
    <button id="hamburger" class="noJavascript" onclick="trigger_menu()">
        <div class="slices_hamburger"></div>
        <div class="slices_hamburger"></div>
        <div class="slices_hamburger"></div>
    </button>
</div>
<!-- end header -->

{$menuH}

<!-- navigation bar -->

<div id="menu_hide">

<div id="breadcrumb">
    <h2 id="position_page">
        {$position}
    </h2>
</div>

<div id="content"><!-- avrà margin: 0 auto; per averlo centrale con larghezza di 720px -->
       {$content}
    </div>
<!-- end content -->

<div id="footer">
    <a id="gotop_help" href="#header">Go to the top</a>

    <p>All rights reserved <span class="print"><br/> Space Travel - https://spacetravelfinal.netlify.com </span></p>
    <div id="valid_w3c">
        <img id="logo_xhtml" alt=""  src="./img/xhtml.png" />
        <img id="logo_css" alt=""  src="./img/css.png" />
        <img id="logo_wai" alt=""  src="./img/wai.png" />
    </div>
</div>

</div>

<script type="text/javascript" src="./js/index.js"></script>

</body></html>

EOD;
}

    /**
     * @param $pageCurrent current page
     * @return string HTML for page menu
     */
    private static function getMenuHtml($pageCurrent)
    {
            $pages['home'] = "index.html";
            $pages['contacts'] = "contacts.html";
            $pages['faq'] = "faq.html";
            $pages['destinations'] = "list1.html";

            $html = <<<EOD
    
            <div id="menu">
                <ul>
                    <li id="close_btn" class="noJavascript">
                    <button onclick="trigger_menu()">
                        <div id="close_cont">
                            <div class="closeslice"></div>
                            <div class="closeslice"></div>
                        </div>
                     </button>
                    </li>

EOD;

            foreach($pages as $page => $url)
            {
                if($pageCurrent == $page) {
                    $html .= '<li><span>' . ucfirst($page) . '</span></li>';
                }
                else {
                    $html .= '<li><a href="' . $url . '">' . ucfirst($page) . '</a></li>';
                }
            }

            $html .= '</ul></div>';
            return $html;
        }
    }