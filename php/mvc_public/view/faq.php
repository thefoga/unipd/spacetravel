<?php

require_once(__DIR__ . '/userLayoutBase.php');

/**
 * generates HTML of the page FAQ
 */
class faq{

    /**
     * @return string FAQ pages
     */
    public static function show(){
        return userLayoutBase::show("FAQ","FAQ","faq","Questions and Answers about space!",self::content());
    }

    /**
     * @return string content of the page FAQ
     */
    private static function content(){
        $html = <<<EOD

 <div class="row">
            <h3 class="subtitle">Questions and Answers!</h3>
            <div id="question_container">
                <div class="question">
                  <p>
                    <span class="strong db">
                        How fast do your rockets go?
                    </span>
                        Around 85% light speed, much faster than your car.
                    </p>
                </div>
                <div class="question">
                  <p>
                    <span class="strong db">
                        What kind of luggage can I bring with me?
                    </span>
                        Each passenger is granted room equivalent to a cube of side 2 metres (8 m<sup>3</sup>).
                        Business class grants you an equivalent of 125 m<sup>3</sup>: you can bring your Tesla with you!
                    </p>
                </div>
                <div class="question">
                  <p>
                    <span class="strong db">
                        Will I be older, younger after traveling with you?
                    </span>
                        Relativistic effects and time dilation can be experienced when travelling at any speed;
                        the guaranteed cruise speed of our rockets is pretty near light speed, but not enough to
                        experience
                        months or even years differences. Our R&amp;D team estimated that you may live 5 minutes longer.
                    </p>
                </div>
                <div class="question">
                  <p>
                    <span class="strong db">
                        Is there any anti-gravity system during the journey?
                    </span>
                        Of course! Except when leaving and landing on a planet, the anti-gravity levels set
                        automatically
                        to ensure an Earth-like experience.
                    </p>
                </div>
                <div class="question">
                  <p>
                    <span class="strong db">
                        Can I use electromagnetic devices (mobile, IPods ...) during the journey?
                    </span>
                        Our passenger bay is equipped with a self-containing electromagnetic shield, so no devices could
                        possibly interfere with the cabin activities: you are free to use any device!
                    </p>
                </div>
                <div class="question">
                  <p>
                    <span class="strong db">
                        I am a very sporty person, will there be available training areas for long journeys?
                    </span>
                        An entire bay is reserved to sports: there is enough room to host two full-size football fields.
                        There is also a 25m-long swimming pool and a spa.
                    </p>
                </div>
                
                 <div class="question">
                  <p>
                    <span class="strong db">
                        Can I bring my children with me?
                    </span>
                        Of course! You can bring your whole family with you, except for your pets.
                        Children of all ages can travel with us, albeit we wouldn't suggest you to bring toddlers for long journeys.
                    </p>
                </div>               
 
                 <div class="question">
                  <p>
                    <span class="strong db">
                        How much it would cost?
                    </span>
                        Per person 5.000$, except for kids under 10 years old who get to travel for free.
                    </p>
                </div>
                
                <div class="question">
                  <p>
                    <span class="strong db">
                        Do we have beds to sleep in during long journeys?
                     </span>
                       Absolutely! Every passenger will have a bed in a cabin.
                       Every cabin can have 2 up to 10 beds.
                       You can choose the one that suits you best.
                       If you are traveling alone, we highly recommend you our largest cabins, as we feel you could get to meet a lot of new people.
                    </p>
                </div>      
                
                <div class="question">
                  <p>
                    <span class="strong db">
                        I am celiac, will be there food I can actually eat?
                    </span>
                        Our chefs are ready for everything, you name it. There are special dishes for celiac, vegetarian
                        and vegan people. Make sure to notify the staff at your arrival!
                    </p>
                </div>
            </div>
        </div>


EOD;

    return $html;

    }

}