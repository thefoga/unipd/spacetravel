<?php

require_once(__DIR__ . '/userLayoutBase.php');

/**
 * generates HTML code of the page Contacts
 */
class contacts{

    /**
     * @return string Contacts page
     */
    public static function show(){
        return userLayoutBase::show("Contact us","Contact us for help","contacts","Chat with the fastest travel agency in the entire galaxy",self::content());
    }

    /**
     * @return string Contacts's page content
     */
    private static function content(){
  $html = <<<EOD

        <div class="row">
            <h3 class="subtitle">Please email us or contact us on social will be in touch with you shortly</h3>
            <ul class="email_list">
                <li>
                    Info or questions at <a href="mailto:info@spacetravel.com">info@spacetravel.com</a>
                </li>
                <li>
                    Destinations and tour <a href="mailto:travel@spacetravel.com">travel@spacetravel.com</a>
                </li>
                <li>
                    Schools or events <a href="mailto:social@spacetravel.com">social@spacetravel.com</a>
                </li>
            </ul>
        </div>
    
        <div class="row">
            <h3 class="subtitle">Our headquarter location</h3>
            <address>
                Building 151 <br/>
                Alpha sector 345<br/>
                Planet OmegaPrimus 678<br/>
            </address>
        </div>
        
        <img class="planet_bigpic" alt="areal photo of our headquarter on OmegaPrimus" src="./img/our_place.png"/>

EOD;

        return $html;

    }

}