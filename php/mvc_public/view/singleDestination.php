<?php
/**
 * generates HTML code of a single destination page
 */
 
class singleDestination
{
    /**
     * @param $dest single destination to display
     * @param $page page where the destination is located, in the destinations list
     * @return string content of the page
     */
    public static function show($dest,$page){

    $descrHtml = "<p>".preg_replace("/\r\n|\r|\n/",'</p><p>',$dest["Description"])."</p>";//new lines to paragraph
    $descrHtml =  str_replace('<p></p>','',html_entity_decode($descrHtml));

$html = <<<EOD

<div class="full_row">
    <img class="planet_bigpic" alt="" src="{$dest['ImgUrl']}"/>
</div>

    <div class="row descr_planet">
        {$descrHtml}
    </div>

    <div class="row messageClass">
        <h3>You want to book this destination?</h3>
        <form action="./php/mvc_admin/api/subscribe.php" method="get">
            <fieldset>
                <label for="email">Email:</label>
                <input id="email" name="email" type="text"/>
                <input name="id" type="hidden" value="{$dest['Id']}"/>
                <input value='Subscribe email' type="submit"/>
            </fieldset>    
        </form>
    </div>

    <a class="link_dest_action nav_dest" href="list{$page}.html">Go back to destination list.</a>
EOD;

return $html;
}

}