<?php

require_once(__DIR__ . '/userLayoutBase.php');

/**
 * generates HTML code of the page HOME
 */
class home{

    /**
     * @return mixed HOME page
     */
    public static function show(){
        $html = userLayoutBase::show("Home "," Home - Travel to new Planets","home","Space traveling is safer than crossing the street.",self::content());
        return str_replace("body_id","bghome",$html);
    }

    /**
     * @return string content of the page HOME
     */
private static function content(){
   $html = <<<EOD

    <div class="home_row">
            <h3 class="subtitle">What's our mission?</h3>
            <p>
                You should visit exotic places like Andromeda-2 or maybe choose a more western world like Mars.<br/>
                Space traveling is safer than crossing the street.
            </p>
    </div>
    
    <div class="home_row">
            <h3 class="subtitle">Do you want to discover more?</h3>
            <p>
                We have been working in the space business for the last 200 years.
                We are adding dozen of new destinations, colonies and exclusive resorts all around our galaxy every year.
                We hope to see you soon on board!
            </p>
    </div>

    <div id="homepics">
        <a href="list1.html">
        <span class="linkepic">
            <img  class="smallpic" src="./img/where_h.png" alt=""/><span class="linktext">Where should I travel to?</span>
        </span>
        </a>
           
        <a href="faq.html">
        <span class="linkepic">
            <img class="smallpic" src="./img/question_h.png" alt=""/><span class="linktext">Questions about space?</span>
        </span>
        </a>
    </div>

EOD;

   return $html;

}

}