<?php

require_once(__DIR__ . '/userLayoutBase.php');

/**
 * generates HTML code of the page if the email submission was successful
 */
class subscribed{

    /**
     * @return string of the page if the email submission was successful
     */
    public static function show(){
        return userLayoutBase::show("Signup successful","Email newsletter destination","subscribed","Email newsletter subscription",self::content());
    }

    /**
     * @return string HTML of the content of the page
     */
    private static function content(){
  $html = <<<EOD

        <div id="destination_parent" class="messageClass messageClass0">
            <h2 class="subtitle">Destination subscription</h2>
            <p id="destination_message">
                You'll get an email when we'll publish this destination trips and dates.
            </p>
            <a id="destination_link" href="list1.html">Go back to destination list</a>
            
        </div>

EOD;

        return $html;

    }

}