<?php

require_once(__DIR__ . '/userLayoutBase.php');

/**
 * generates HTML code of the page in case of failed submission of the email
 */
class notSubscribed{

    /**
     * @return string of the page in case of failed submission of the email
     */
    public static function show(){
        return userLayoutBase::show("Signup impossible","Email newsletter destination","notSubscribed","Email newsletter subscription",self::content());
    }

    /**
     * @return string HTML content of the page
     */
    private static function content(){
  $html = <<<EOD

        <div id="destination_parent" class="messageClass messageClass2">
            <h2 class="subtitle">Destination subscription</h2>
            <p id="destination_message">
                We couldn't save your email, you might have already sent a request for this specific 
                destination with this email.
                Or you might have sent us a fake email.
            </p>
            <a id="destination_link" href="list1.html">Go back to destination list</a>
            
        </div>

EOD;

        return $html;

    }

}