<?php

/**
 * generates code of the destinations list pages
 */
class userListDestination
{

    /**
     * generates code of the page
     * @param $dest_pagina destination in a page
     * @param $nPagina number of current page
     * @param $nPagine number of total pages
     * @return string HTML code of destinations list page
     */
    public static function show($dest_pagina,$nPagina,$nPagine){
        $html = "<form id=\"search_bar\" class='link_dest_action full_row' action='./php/mvc_admin/api/search.php' method='get'>
    <fieldset>
    <legend>Search a destination</legend><label for=\"searchtext\">Search:</label><input id=\"searchtext\" name=\"searchtext\" type=\"text\"/>
    <input value='Search' type=\"submit\"/>
    </fieldset></form>";
        $html .= "<div id='dest_list'>" .self::buildDestinations($dest_pagina)."</div>";
        $html .= self::buildNavigationPages($nPagina,$nPagine);
        return $html;
    }


    /**
     * generates content of the page
     * @param $destinazioni destinations in the destrination list page
     * @return string HTML code of the destination in the destination list
     */
    private static function buildDestinations($destinazioni){
        $html = '';
        foreach ($destinazioni as $destV){
            $dest = get_object_vars($destV);
            $descr = substr($dest["Description"],0,160);
            $html .= <<<EOD
            <div class="destination">
            <span class="left">
                   <img class="leftimg" src="{$dest["ImgUrl"]}" alt=""/>
            </span>
            <span class="right">
                   <a title="More info about {$dest["Title"]}" href="dest{$dest["Id"]}.html">
                        <strong>{$dest["Title"]}</strong>
                   </a>
                   <span class="dest_text">{$descr}...</span>
             </span>
            </div>
EOD;

        }
    return $html;
 }

    /**
     * handles the navigation area of the destinations list pages
     * @param $nPagina number of the current page
     * @param $pagine number of total pages
     * @return string code of the changes applied to the navigation area
     */
    private static function buildNavigationPages($nPagina,$pagine){
        $html = '';
        if($nPagina > 1) {
            $html .= '<a class="link_dest_action nav_dest" href="list' . ($nPagina - 1) . '.html">&lt; Go to previous page</a>';
        }else {
            $html .= '<span class="link_dest_action nav_dest">&lt; Go to previous page</span>';
        }

        if($nPagina < $pagine) {
            $html .= '<a class="link_dest_action nav_dest" href="list' . ($nPagina + 1) . '.html">Go to next page &gt;</a>';
        }else {
            $html .= '<span class="link_dest_action nav_dest">Go to next page &gt;</span>';
        }

        $html .="<p class=\"link_dest_action nav_dest\" >Page ".$nPagina." of ".$pagine."</p><form id=\"nav_pages\" class=\"link_dest_action nav_dest\" action='./php/mvc_admin/api/gotodest.php' method='get'><fieldset><label for=\"page\">Go to page</label><input id=\"page\" name=\"page\" type=\"text\"/><input value='Go' type=\"submit\"/></fieldset></form>";

        $html .="<span class='hide' id='numPages'>".$pagine."</span>";

        return $html;
    }
}