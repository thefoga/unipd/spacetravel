<?php

/**
 * Cleans the public html files from the public folder
 */
function cleanPublic()
{
    $files = glob('../../*'); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file)&& contains($file,".html")) {
            unlink($file); // delete file
        }
    }
}


/**
 * Checks if $needle contains haystack
 * @param $stringToLookOn string to look on
 * @param $stringToFind string to find inside the other one
 * @return bool TRUE if found FALSE if not found
 */
function contains($stringToLookOn, $stringToFind)
{
    return (strpos($stringToLookOn, $stringToFind) !== false);
}
