<?php

require_once __DIR__. '/model/remoteDestinations.php';
require_once __DIR__. '/view/singleDestination.php';
require_once __DIR__. '/view/userListDestination.php';
require_once __DIR__. '/view/userLayoutBase.php';
require_once __DIR__. '/view/home.php';
require_once __DIR__. '/view/faq.php';
require_once __DIR__. '/view/contacts.php';
require_once __DIR__. '/view/subscribed.php';
require_once __DIR__. '/view/notSubscribed.php';

require_once __DIR__. '/clean.php';

build::rightNow();

/**
 * Builds all the external user/travellers website pages fetching the API backend an parsing the JSON as
 * object that will use to build list pages and specific destinations
 */
class build{
    /**
    * builds all the destination list
    * @param $dest destinations
    * @param $nP number of pages
    * @param $dimP number of destinations per page
    */
    public static function rightNow()
    {
        cleanPublic();

        $rD = new remoteDestinations();

        $dest = $rD->destinations;
        $nP = $rD->n_pagine;
        $dimP = $rD->dim_pagine;

        for($pag=1; $pag <= $nP; $pag++)
        {
            $dest_pagina = [];
            $incr = ($pag-1)* $dimP;
            for($desti=1; $desti <= $dimP && ($incr+$desti) <= count($dest); $desti++){
                $dest_pagina[] = $dest[$desti-1+$incr];
                build::buildDestination($dest[$desti-1+$incr],$pag);
            }
            build::buildDestinationsPage($dest_pagina,$pag,$nP);
        }

        file_put_contents("../../index.html", home::show());
        file_put_contents("../../faq.html", faq::show());
        file_put_contents("../../contacts.html", contacts::show());
        file_put_contents("../../subscribed.html", subscribed::show());
        file_put_contents("../../notSubscribed.html", notSubscribed::show());


        if(isset($_GET['goto'])) {
            header('Location: ../mvc_admin/' . $_GET['goto']);
            exit();
        }
        header('Location: ../mvc_admin/list.php');
    }

    /**
     * builds destination
     * @param $destV primitive destination, not yet an associative array
     * @param $page page of the destination
     */
    private static function buildDestination($destV, $page)
    {
        $dest = get_object_vars($destV);
        $filname = "../../dest".$dest['Id'].".html";
        file_put_contents($filname, userLayoutBase::show($dest["Title"],$dest["Title"],"dest.html","Visit and travel to ".$dest["Title"],singleDestination::show($dest,$page)));
    }

    /**
     * builds the destinations list page
     * @param $dest_pagina page of the destinations list
     * @param $pag current page
     * @param $npagine total number of pages
     */
    private static function buildDestinationsPage($dest_pagina,$pag,$npagine)
    {
        $filname = "../../list".$pag.".html";
        $tp = ("Destinations list page ".$pag);
        file_put_contents($filname, userLayoutBase::show( $tp ,$tp,"list.html","Discover new destinations all around the universe", userListDestination::show($dest_pagina,$pag,$npagine)) );
    }

}
