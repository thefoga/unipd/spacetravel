<?php
/**
 * fetches from the API backend JSON file and integer of dimension of the pages and number of pages
 */
class remoteDestinations
{

    public $n_pagine;
    public $dim_pagine;
    public $destinations;
    /**
    * fetches from the API backend JSON file and integer of dimension of the pages and number of pages
    * and saves the informations
    */
    public function __construct()
    {

        $url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $url = (explode("mvc_public",$url));
        $url = $url[0];

        $pages = "mvc_admin/api/dest_list.php?action=getPages";
        $dimPagine = "mvc_admin/api/dest_list.php?action=getDimPages";
        $getAll = "mvc_admin/api/dest_list.php?action=getAll";


        $this->n_pagine = file_get_contents('http://'.$url.$pages);
        $this->dim_pagine = file_get_contents('http://'.$url.$dimPagine);
        $destinationsJson = file_get_contents('http://'.$url.$getAll);

        $arr = (json_decode($destinationsJson));

        $obj = [];

        foreach ($arr as $val => $str) {
            $obj[] = json_decode($str);
        }

        $this->destinations = $obj;
    }



}