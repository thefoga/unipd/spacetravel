<?php

require_once __DIR__ . '/model/session.php';
require_once __DIR__ . '/model/destination.php';
require_once __DIR__ . '/view/showMessage.php';
require_once __DIR__ . '/view/editDestination.php';
require_once __DIR__ . '/view/layoutBase.php';
require_once __DIR__ . '/view/actionInfo.php';

$session = new session();
$session->createSession();
if (!$session->isLogged()) {
    header('Location: login.php?info=needLogin');
    exit();
}
    $html ="";
    if (isset($_GET["info"])) {
        $html =  actionInfo::show($_GET["info"]);
    }
    echo layoutBase::show("Edit Modify Destination Space Travel", "Modify Destination", "edit", "", $html.show() );

/**
 * @return string HTML with destination page
 */
function show(){
    $destination = new destination(-1, "", "", "");

    if (isset($_REQUEST["action"])) {
        $action = $_REQUEST["action"];

        if (isset($_REQUEST["num"])&& $_REQUEST["num"] != -1 ) {
                $destination = destination::load($_REQUEST["num"]);
                if(!is_object($destination)) {
                    return showMessage::show("Destination not found, someone deleted this one or you are hacking the system.", "What Id are you looking at?");
                }
            return handleDestination($action, $destination);
        }

        if(allFieldsSet($destination) && save($destination)){ //id not defined but all field set save it
            header('Location: ../mvc_public/build.php?goto=list.php?info=inserted');
        }else{
            return showMessage::show("Set all fields to create a new destination!","Not created").editDestination::show($destination);
        }

    }

    return  editDestination::show(($destination));
}

/**
 * @param $action action on the destination to edit
 * @param $destination the specific object to change, delete
 * @return string raw HTML with destination page
 */
function handleDestination($action, $destination){
    $html ="";
switch ($action) {
    case 'delete':
        $destination->delete();
        header('Location: ../mvc_public/build.php?goto=list.php?info=deleted');
        break;
    case 'modify':
        if(atLeastOneFieldSet($destination) && save($destination)) {
            header('Location: ../mvc_public/build.php?goto=list.php?info=saved');
        }else{
            header('Location: ./edit.php?info=modify_something&num='.$destination->getId().'&action=edit&info=fixContent' );
        }
        $html .= editDestination::show($destination);
        break;
    case 'edit':
    default:
        $html .= editDestination::show($destination);
        break;
}  //switch action
    return $html;
}


/**
 * @param $destination destination to check
 * @return bool true iff all destination fields are valid and correct (checked)
 */
function allFieldsSet($destination){
    return checkTitle($destination)&&checkDescr($destination)&&checkImgUrl($destination);
}

/**
 * @param $destination destination to check
 * @return bool true iff at least one destination field is valid and correct
 */
function atLeastOneFieldSet($destination){
    $atLeast = 0;
    $atLeast += checkTitle($destination);
    $atLeast += checkDescr($destination);
    $atLeast += checkImgUrl($destination);
    return($atLeast > 0);
}

/**
 * Modify destination according to page passed values and then save to db
 * @param $destination destination object to save
 * @return int 1 if saved 0 if not saved
 */
function save($destination)
{
        return $destination->save();
}

/**
 * @param $destination destination object to check
 * @return bool true iff title of destination is valid
 */
function checkTitle($destination){
     return (isset($_POST["planet"]) && $destination->getTitle()!= $_POST["planet"] && $destination->setTitle($_POST["planet"]));
}

/**
 * @param $destination destination object to check
 * @return bool true iff description of destination is valid
 */
function checkDescr($destination){
    return (isset($_POST["planet_description"]) && $destination->getDescription()!= htmlentities($_POST["planet_description"]) && $destination->setDescription($_POST["planet_description"]));
}

/**
 * @param $destination destination object to check
 * @return bool true iff url of image of destination is valid
 */
function checkImgUrl($destination){
    return (isset($_POST["imageurl"]) && $destination->getImgUrl()!= $_POST["imageurl"] && $destination->setImgUrl($_POST["imageurl"]));
}