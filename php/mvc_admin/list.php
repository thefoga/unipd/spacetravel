<?php

require_once(__DIR__ . '/model/destination_list.php');
require_once(__DIR__ . '/view/listDestination.php');
require_once(__DIR__ . '/view/showMessage.php');
require_once(__DIR__ . '/model/session.php');
require_once(__DIR__ . '/view/layoutBase.php');
require_once(__DIR__ . '/view/actionInfo.php');

$session = new session();
$session->createSession();

if (!$session->isLogged()) {
    header('Location: login.php?info=needLogin');
    exit();
}

$title_breadcrumb = "List destinations";
$menu_pos = "list";
$html = show();

if (isset($_GET["info"])) {
    $html = actionInfo::show($_GET["info"]) . $html;
}

    echo layoutBase::show($title_breadcrumb, $title_breadcrumb, $menu_pos, "seo", $html);

/**
 * @return string raw HTML with destination list
 */
function show()
{
    $destinationList = new destination_list();

    $n_pagine = $destinationList->getPages();

    if ($n_pagine === 0) {
        return showMessage::show("No destinations in the system <a href='edit.php?action=create'>Add the first destination</a>", "Let's start adding some places!");
    }

    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = 1;
    }

    if ($page < 1 || $page > $n_pagine) {
        return showMessage::show("Page not found, go back to main page of listing. <a href='list.php'>Go back to first page.</a>","You are visiting a page out of bounds!",2);
    }

    $dest = $destinationList->getPage($page);
    return listDestination::show($dest, $page, $n_pagine);
}