<?php

require_once __DIR__ . '/model/destination_list.php';
require_once __DIR__ . '/view/listDestination.php';
require_once __DIR__ . '/view/showMessage.php';
require_once __DIR__ . '/model/session.php';
require_once __DIR__ . '/view/layoutBase.php';
require_once __DIR__ . '/view/actionInfo.php';

$session = new session();
$session->createSession();

if (!$session->isLogged()) {
    header('Location: login.php?info=needLogin');
    exit();
}

$title_breadcrumb = "Searching destinations";
$menu_pos = "search";
$html = show();

if (isset($_GET["info"])) {
    $html = actionInfo::show($_GET["info"]) . $html;
}
$html =  str_replace('<p></p>','',html_entity_decode($html));

    echo layoutBase::show($title_breadcrumb, $title_breadcrumb, $menu_pos, "seo", $html);

/**
 * @return string raw HTML with search results
 */
function show()
{
    $sql_search = "";
    if(isset($_GET["searchtext"]) && $_GET["searchtext"] != ""){
        $searched = $_GET["searchtext"];
        $words = preg_split('/ /',$searched);
        foreach ($words as $word){
            $sql_search .=$word . "|";
        }
        $sql_search = substr($sql_search,0,-1);
    }else{
        return showMessage::show("You need to search something</p>".layoutBase::searchBar()."<p>","No words searched!",2);
    }

    $destinationList = new destination_list($sql_search);

    $n_pagine = $destinationList->getPages();

    if ($n_pagine == 0) {
        return showMessage::show("No destinations found for your keywords change them</p>".layoutBase::searchBar($searched)."<p>", "Our robots didn't find what you were looking for");
    }

    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = 1;
    }

    if ($page < 1 || $page > $n_pagine) {
        echo $n_pagine;
        return showMessage::show("No results found, search another term</p>".layoutBase::searchBar($searched)."<p>","Our robots didn't find what you were looking for");
    } else {
        $dest = $destinationList->getPage($page);
        return showMessage::show("We found so many matches!","Search results!",0).listDestination::show($dest, $page, $n_pagine,urlencode($searched));
    }
}


//SELECT * FROM destinations WHERE Title REGEXP 'mars|pluto' OR Description
