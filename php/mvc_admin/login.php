<?php

require_once(__DIR__ . "/model/session.php");
require_once(__DIR__ . "/view/loginUser.php");
require_once(__DIR__ . "/view/layoutBase.php");
require_once(__DIR__ . "/view/actionInfo.php");

$session = new session();
$session->createSession();

if ($session->isLogged()) {
    header('Location: list.php?info=alreadyLoggedIn');
    exit();
}

$html = loginUser::show();

if(isset($_GET["info"])) {
    $html = actionInfo::show($_GET["info"]) . $html;
}

echo layoutBase::show("Login to Space Travel Admin", "LOGIN", "login", "", $html);


if (isset($_POST['username']) && isset($_POST['password'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (strlen($username) > 0 && strlen($password) > 0) {
        $userId = $session->canLogUser($username, $password);
        if ($userId >= 0) {
            header('Location: list.php?info=loggedIn');
        }else{
            header('Location: login.php?info=wrongCredentials');
        }
    }
}

  