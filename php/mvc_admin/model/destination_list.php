<?php

require_once (__DIR__.'/db.php');
require_once (__DIR__.'/destination.php');


/**
 * Container of list of destinations
 */
class destination_list
{

    private $conn;

    private $sqlPage = ' SELECT * FROM destinations ORDER BY Id DESC';
    private $sqlAll = ' SELECT * FROM destinations ORDER BY Id DESC';
    private $sqlPages = ' SELECT COUNT(*) as npagine FROM destinations ORDER BY Id DESC';

    /**
     * @return int how many destinations per page
     */
    public static function dimPagina(){
        return 4; //destinazioni per ogni pagina
    }

    /**
     * Builds new list of destination
     * @param string $searchwhat search query
     */
    public function  __construct($searchwhat = "")
    {
        $this->conn = new db();
        $this->conn->connect();

        if($searchwhat != ""){
            $this->sqlPage = "SELECT *, MATCH (Title) AGAINST ( '".$searchwhat."' IN NATURAL LANGUAGE MODE) AS title_match, MATCH (Description) AGAINST ('".$searchwhat."'  IN NATURAL LANGUAGE MODE) as descr_match 
            FROM `destinations` 
            WHERE Title LIKE '%".$searchwhat."%' OR  Description LIKE '%".$searchwhat."%'
            ORDER BY title_match DESC, descr_match DESC ,Id DESC";
            $this->sqlAll =$this->sqlPage;
            $this->sqlPages = "SELECT COUNT(*) as npagine FROM `destinations` WHERE Title LIKE '%".$searchwhat."%' OR  Description LIKE '%".$searchwhat."%' ";
        }
     }


     //SELECT *, MATCH (Title) AGAINST ('mars' IN NATURAL LANGUAGE MODE) AS title_match, MATCH (Description) AGAINST ('mars' IN NATURAL LANGUAGE MODE) as descr_match FROM `destinations` WHERE MATCH (Title) AGAINST ('mars' IN NATURAL LANGUAGE MODE) > 0 OR MATCH (Description) AGAINST ('mars' IN NATURAL LANGUAGE MODE) > 0 ORDER BY title_match DESC, descr_match DESC ,Id DESC

    /**
     * @return float|int total number of pages of list
     */
    public function getPages()
    {
        $res = $this->conn->select($this->sqlPages);
        $result =$res->fetch_assoc();
        if($result){
            return ceil($result["npagine"]/$this::dimPagina());
        }
        return -1;//no result means no pages
    }

    /**
     * @return array list of all destinations (json format)
     */
    public function getAll()
    {
        $result = $this->conn->select($this->sqlAll);
        $destinations = [];
        while ($query_row = $result->fetch_assoc()) {
            $destL = destination::build($query_row);
            $destinations[] = $destL->getJSON();
        }
        return $destinations;
    }

    /**
     * @param $page number of page
     * @return array list of destination in page
     */
    public function getPage($page)
    {
            $sql = $this->sqlPage." LIMIT ".$this::dimPagina()." OFFSET " . ($page-1) * $this::dimPagina();

            $result = $this->conn->select($sql);
            $destinations = [];
            while ($query_row = $result->fetch_assoc()) {
                $destinations[] = destination::build($query_row);
            }
            return $destinations;
    }

    /**
     * @param $result raw data
     * @return destination new destination with id, title, image and description from raw data
     */
    public static function build($result)
    {
        return new destination($result["Id"],$result["Title"],$result["ImgUrl"],$result["Description"]);
    }

}