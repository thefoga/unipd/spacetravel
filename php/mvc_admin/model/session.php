<?php

include "user.php";
require_once "db.php";

/**
 * Handles users login sessions
 */
class session
{
    /**
     * Starts new session
     */
    public function createSession()
    {
        session_start();
    }

    /**
     * @param $username username to login
     * @param $password password used to login
     * @return mixed|string true iff user auth has completed successfully
     */
    public function canLogUser($username,$password){
        $sql = 'SELECT * FROM users WHERE Username = "'.$username.'"';
        $conn = new db();
        if($conn->connect()){
            $res = $conn->select($sql);
            $result =$res->fetch_assoc();
            if ($result) {
                $verify = ($password === $result['Password']);
                echo $password." ". $result['Password'];
                if ($verify) {
                    return $this->logUser($result['Id']);
                } else {
                    return  "{\"message\":\"login password error\"}";
                }
            } else {
                return  "{\"message\":\"login user not found\"}";
            }
        }
    }


    /**
     * @param $userId id of user to be logged in
     * @return mixed actually log user in
     */
    public function logUser($userId)
    {

        $_SESSION['userId'] = $userId;

        return  $userId;

    }

    /**
     * @return bool true iff user is already logged in
     */
    public function isLogged()
    {
        if (isset($_SESSION['userId'])) {
            return true;
        }
        return false;
    }

    /**
     * @param $userId perform re-log
     */
    public function relogUser($userId)
    {
        $_SESSION['userId'] = $userId;
    }

    /**
     * Removes user session (logs out)
     */
    public function logOut(){
        unset($_SESSION['userId']);
        setcookie('authToken', '', 1);
        unset($_COOKIE['authToken']);
    }

    /**
     * @return mixed id of user logged in
     */
    public function getId()
    {
        return $_SESSION['userId'];
    }
}