<?php

require_once __DIR__. '/db.php';

/**
 * Models destination object
 */
class destination
{

private $Id;
private $Title;
private $ImgUrl;
private $Description;

    public function  __construct($I,$T,$IU,$D)
    {
        $this->Id = $I;
        $this->Title = $T;
        $this->ImgUrl = $IU;
        $this->Description = $D;
    }

    /**
     * @return id of destination
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param $Id id of destination to set
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string title of destination
     */
    public function getTitle()
    {
        return $this->Title;
    }

    /**
     * @param $Title title of destination to set
     * @return bool true iff operation was successfully carried out
     */
    public function setTitle($Title)
    {
        if(preg_match( '/^[\w ]+$/', $Title)) {
            $this->Title = $Title;
            return true;
        }
        return false;
    }


    /**
     * @return string url of image of destination
     */
    public function getImgUrl()
    {
        return $this->ImgUrl;
    }

    /**
     * @param $ImgUrl url of image of destination to set
     * @return bool true iff operation was successfully carried out
     */
    public function setImgUrl($ImgUrl)
    {
        if (@getimagesize($ImgUrl) || filter_var($ImgUrl, FILTER_VALIDATE_URL)) {
            $this->ImgUrl = $ImgUrl;
            return true;
        }
        return false;
    }

    /**
     * @return string description of destination
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param $Description description of destination to set
     * @return bool true iff operation was successfully carried out
     */
    public function setDescription($Description)
    {
        $this->Description = htmlentities($Description);
        return true;
    }

    /** Load the destination to
     * @param int Id of the destination to load
     * @return destination|int destination iff operation was successfully carried out, else -1
     */
    public static function load($Id){
        $sql = "SELECT * FROM destinations WHERE Id = $Id";

        $conn = new db();
        if($conn->connect()){
            $res = $conn->select($sql);
            $result =$res->fetch_assoc();
            if ($result) {
                return new destination($result["Id"], $result["Title"], $result["ImgUrl"], $result["Description"]);
            }
        }

        return -1;
    }

    /**
     * @param $result raw data
     * @return destination new destination with id, title, image and description taken from raw data
     */
    public static function build($result){
        return new destination($result["Id"],$result["Title"],$result["ImgUrl"],$result["Description"]);
    }

    /**
     * Delete the destination in the DB
     * @param int Id of the destination to delete
     * @return int|string message iff operation was successfully carried out, else 0
     */
    public function delete(){
        $sql = "DELETE FROM destinations WHERE Id = $this->Id";

        $conn = new db();
        if($conn->connect()){
            $result = $conn->delete($sql);
            if($result == 1){
                return  "{\"message\":\"destination ".$this->Id." deleted\"}";
            }

            return $result;
        }

        return 0;
    }

    /**
     * Save the destination to DB
     * @return string or Id of inserted
     */
    public function save(){
    $retu = 0;

        if(self::load($this->Id) === -1){ //create new destination and get his new Id
            $sql = "INSERT INTO destinations (Title, ImgUrl, Description) VALUES ( '$this->Title', '$this->ImgUrl' , '$this->Description' );";
            $conn = new db();
            if($conn->connect()){
                $result = $conn->insert($sql);
                if($result == 1){
                    $retu =  $this->getLastIdInserted($conn);
                }
            }
        }
        else{//Update a destination with Id
            $sql = "UPDATE destinations SET Title = '$this->Title', ImgUrl = '$this->ImgUrl' , Description = '$this->Description' WHERE Id = $this->Id ;";
            $conn = new db();
            if($conn->connect()){
                $result = $conn->update($sql);
                if($result == 1){
                    $retu =  1;
                }
            }
        }

        return $retu;
    }

    /**
     * @param $conn database connection
     * @return string message with last destination id inserted
     */
    private function getLastIdInserted($conn){
        $sql="SELECT LAST_INSERT_ID() AS Id FROM destinations LIMIT 1";
        $res = $conn->select($sql);
        $result =$res->fetch_assoc();
        if($result){
            return  1;
        }

        return 0;
    }

    /**
     * Return a JSON object string with all destination data
     * @return string json with obj representation
     */
    public function getJSON(){
      $arrayObj  = get_object_vars($this);
      return json_encode($arrayObj);
    }

}


