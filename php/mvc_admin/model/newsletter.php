<?php

require_once (__DIR__.'/db.php');
require_once (__DIR__.'/destination.php');

/**
 * Manages users newsletter
 */
class newsletter{

    private $email;
    private $id_destination;
    private $esitoBool;

    /**
     * Builds new newsletter with user email
     * @param $email user email (where to send emails)
     * @param $id_dest (destination to send info about)
     */
    public function __construct($email,$id_dest){

        $this->email = $email;
        $this->id_destination = $id_dest;
        $this->esitoBool = false;

        if($this->checkExistsDestination() && $this->checkEmailFormat() && $this->checkEmailDns() && $this->checkEmailHunterIo() && $this->notAlreadySent() && $this->sendToDB()) {
            //"We received your request! Don't worry you'll get notified when we'll publish destination trips and dates!"
            $this->esitoBool = true;
        }

        //"You might have already sent this preference or the email is not valid."
    }

    /**
     * Check if the destination id esists in the Model DB
     * @return bool true if destination actually exists
     */
    private function checkExistsDestination(){
        return !(destination::load($this->id_destination) === -1) ;
    }

    /**
     * Check is the email content is correct with Regular Expression
     * @return bool true if email is a valid one
     */
    private function checkEmailFormat(){
        return (bool)preg_match("`^[a-z0-9!" . "#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($this->email));
    }

    /**
     * Check if the email wasn't already sent for that destination
     * @return int 0 if email has been sent, 1 if never sent
     */
    private function notAlreadySent(){
        $sql = "SELECT * FROM newsletter WHERE Id = $this->id_destination AND Email = '$this->email'" ;

        $conn = new db();
        if($conn->connect()){
            $res = $conn->select($sql);
            $result =$res->fetch_assoc();
            if ($result) {
                return 0;  //email present already
            }
        }

        return 1;

    }

    /**
     * Check if the email is correct and valid using hunter.io
     * @return int 1 if valid email to be sent
     */
    private function checkEmailHunterIo(){
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n"
            )
        );

        $context = stream_context_create($opts);


        $fp = fopen('https://api.hunter.io/v2/email-verifier?email='.$this->email.'&api_key=a319187cc8f90bf9d20fe5f0b39e948059424c97', 'r', false, $context);


        if ($fp) {
            $emailObj = json_decode(stream_get_contents($fp, -1, 0));
            fclose($fp);
            if( $emailObj->data->score > 70 && ($emailObj->data->disposable === false)) {
                return 1;
            }

            return 0; //email not valid


        }

        return 1;//connection to hunter.io not available

    }

    /**
     * @return int 1 if logging newsletter to db has been completed successfully
     */
    private function sendToDB(){
        $sql = "INSERT INTO newsletter (Id, Email) VALUES ( '$this->id_destination', '$this->email' );";
        $conn = new db();
        if($conn->connect()){
            $result = $conn->insert($sql);
            if($result == 1){
                return  1; //Inserted email in the DB
            }
        }
        return 0;//DB was offline or couldn't insert the email
    }

    /**
     * Check if the email domain MX address is set, if not set the domain can't handle emails
     * @return bool|int checks email DNS servers
     */
    private function checkEmailDns(){

            if(filter_var($this->email, FILTER_VALIDATE_EMAIL) === false){
                return FALSE;
            }
            $domain = explode("@", $this->email, 2);
            if(checkdnsrr($domain[1],'MX')) {
                return 1; //valid MX DNS the email can be saved
            }

        return 0;//not valid MX DNS

    }

    /**
     * @return bool true iff result was successful
     */
    public function getEsitoBool()
    {
        return $this->esitoBool;
    }

}