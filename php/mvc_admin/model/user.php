<?php

require_once "db.php";


/**
 * Manages site user
 */
class user
{

    private $username;
    private $id;

    /**
     * Adds new user to database
     * @param $userId id of new user
     */
    public function createUser($userId)
    {
        $conn = new db();
        if ($conn->connect()) {
            $sql = "SELECT * FROM users WHERE Id = '$userId'";
            $results = $conn->select($sql);
            echo
            $this->username = $results['Username'];
            $this->id = $results['Id'];

        }
    }

    /**
     * @return mixed user id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed user username
     */
    public function getUsername()
    {
        return $this->username;
    }

}