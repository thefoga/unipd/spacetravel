<?php

/**
 * Models database getters/setters
 */
class db {

    protected $db_name = 'rgiorato';
    protected $db_user = 'rgiorato';
    protected $db_pass = 'laeshei1iequ4iSi';
    protected $db_host;

    private $connect_db;

    /**
     * Open a connect to the database
     */
    public function __construct(){
        ini_set('error_reporting', 'E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR');

        $this->db_host = $_SERVER['SERVER_ADDR'];
        if($this->db_host === "127.0.0.1" || $this->db_host === "192.168.1.81" ){
            $this->db_host = "127.0.0.1";
            $this->db_name = 'space';
            $this->db_user = 'root';
            $this->db_pass = '';
        }else{
            $this->db_host = "localhost";
            //mi trovo su server tecweb paolotti
            //echo gethostbyname ("127.0.0.1");
        }


            $this->connect_db = new mysqli( $this->db_host, $this->db_user, $this->db_pass, $this->db_name );
        if ($this->connect_db ->connect_error) {

            echo "<h1>Server DB - offline</h1>";
        }
    }

    /**
     * Tries to connect to database
     * @return bool True iff connection was successful
     */
    public function connect() {
        if ( mysqli_connect_errno() ) {
            printf("Connection failed: %s\ ", mysqli_connect_error());
            exit();
        }
        return true;
    }

    /**
     * Insert operation in the database
     * @param string $sql to execute as insert in the db
     * @return int|string raw result of insertion
     */
    public function insert($sql){
        return $this->operate($sql,"insert not possible");
    }

    /**
     * Delete operation in the database
     * @param string $sql to execute as delete in the db
     * @return int|string raw result of deletion
     */
    public function delete($sql){
        return $this->operate($sql,"delete not possible");
    }

    /**
     * Update operation in the database
     * @param string $sql to execute as update in the db
     * @return int|string raw result of update
     */
    public function update($sql){
        return $this->operate($sql,"update not possible");
    }

    /**
     * Select operation in the database
     * @param string $sql to execute as select in the db
     * @return int if query has been successfully executed
     */
    public function select($sql){
        return $this->connect_db->query($sql);
    }

    /**
     * @param $sql operation in the database
     * @param $mess Error message to show in case of error
     * @return int|string  raw result of query
     */
    private function operate($sql,$mess){
        if ($this->connect_db->query($sql) === TRUE) {
            return 1;
        }

        return "{\"Error\":\"".$mess."\"}";//else error
    }

    /**
     * Close the mysqli connection to the database
     */
    public function close(){
        mysqli_close($this->connect_db);
    }
}
