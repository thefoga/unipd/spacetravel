<?php

require_once (__DIR__.'/../model/db.php');

if(isset($_GET["num"])){
    $id = $_GET["num"];

    $csv_filename = 'db_export_'.$id.'_'.date('Y-m-d').'.csv';

    $sql = "SELECT Email FROM newsletter WHERE Id = $id";

    $csv_export = "email subscriptions destination ID: ".$id.";
";

    $conn = new db();
    if($conn->connect()){
        $res = $conn->select($sql);
        while($result =$res->fetch_assoc()) {
            $csv_export .= $result["Email"] . ";
";
        }

    }

    // Export the data and prompt a csv file for download
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header("Content-Disposition: attachment; filename=".$csv_filename."");
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    echo($csv_export);
}
