<?php

require_once __DIR__ . '/../model/destination_list.php';

if(isset($_GET["searchtext"]) && $_GET["searchtext"] != "")
{
    $preHtml = <<<EOD

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Script-Type" content="text/javascript">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Searching destination</title>
        <link href="../../../css/style.css" rel="stylesheet">
        <link rel="icon" type="image/png" href="../../../img/logo.png">
    </head>
    <body>
    <div id='content'>

EOD;

    echo $preHtml;

    $sql_search = "";
    $searched = filter_var($_GET['searchtext'], FILTER_SANITIZE_STRING);
    $words = preg_split('/ /',$searched);
    foreach ($words as $word){
        $sql_search .=$word . "|";
    }
    $sql_search = substr($sql_search,0,-1);

    $destList = new destination_list($sql_search);

    if($destList->getPages() > 0 ) {
        $html = listArray($destList->getPage(1));
    }
    else {
        $html =  "<div id=\"breadcrumb\"> <h2 id=\"position_page\"> No results found for your search </h2>  </div> <div class='noPlanets'></div>";
    }

    if(isset($_SERVER['HTTP_REFERER'])) {
        $html .= "<a  class=\"link_dest_action full_row\" href='" . $_SERVER['HTTP_REFERER'] . "'>Go back to previous page</a>";
    }else{
        $html .= "<a  class=\"link_dest_action full_row\" href='../../../list1.html'>Go back to destination list</a>";
    }
    echo $html."</body></div>";
}
else {
    if(isset($_SERVER['HTTP_REFERER'])) {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    else {
        header('Location: ../../../list1.html');
    }
}




/**
 * Creates HTML div with destination list
 * @param $destinazioni list with destinations
 * @return string HTML code to show destinations from list
 */
function listArray($destinazioni){
    $html ="<div id=\"breadcrumb\"> <h2 id=\"position_page\"> Search results </h2> </div>";

    foreach ($destinazioni as $dest ){
        $name = $dest->getTitle();
        $id = $dest->getId();
        $html .= "<div class=\"destination \"><h3><a href='../../../dest".$id.".html' >".$name."</a></h3></div>";
    }

    return $html;
}