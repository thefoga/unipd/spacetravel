<?php

require_once (__DIR__.'/../model/newsletter.php');

if(isset($_GET["email"]) && isset($_GET["id"])) {
    $email = filter_var($_GET["email"], FILTER_SANITIZE_EMAIL);
    $id_dest = filter_var($_GET["id"],FILTER_SANITIZE_NUMBER_INT);

    $newsletter = new newsletter($email,$id_dest);

    if($newsletter->getEsitoBool() == true) {
        header('Location: ../../../subscribed.html?id=' . $id_dest);
    }else {
        header('Location: ../../../notSubscribed.html?id=' . $id_dest);
    }

    exit();

}


