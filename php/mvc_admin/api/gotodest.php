<?php

if(isset($_GET["page"])){
    $page = filter_var($_GET["page"],FILTER_SANITIZE_NUMBER_INT);
    if(is_int($page)) {
        header('Location: ../../../list' . $page . '.html');
    }
    else {
        header('Location: ../../../list1.html');
    }
}