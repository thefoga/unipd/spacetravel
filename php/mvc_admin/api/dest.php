<?php

require_once (__DIR__. '/../model/destination.php');
require_once (__DIR__ .'/../view/showMessage.php');

/**
 * Models destination
 */
class dest
{
    private $action;
    private $id;
    private $destinazione;

    /**
     * Builds new destination model
     */
    public function  __construct()
    {
        if(isset($_GET["action"])) {
            $this->action = $_GET["action"];
        }
        if(isset($_GET["planet_id"])){
            $this->id = $_GET["planet_id"];
            $this->destinazione = destination::load($this->id);

        if(!$this->destinazione) {
            echo showMessage::show("{\"error\":\"destination not found\"}");
        }
        else{
            switch ($this->action){
                case "show":
                    echo editDestination::show($this->destinazione);
                    break;
                case "get":
                    header('Content-Type: application/json');
                    echo $this->destinazione->getJSON();
                    break;
                default:
                    echo showMessage::show("{\"error\":\"action not found\"}");
                    break;
            }//switch action
        }//else
        }//exists id
    }

}

$dest = new dest();