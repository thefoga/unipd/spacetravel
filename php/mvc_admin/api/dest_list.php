<?php

require_once (__DIR__.'/../model/destination_list.php');

/**
 * Gets list of destinations
 */
class dest_list
{

    private $dList;

    /**
     * Builds new object, setting right message to display
     */
    public function  __construct()
    {
        $conn = new db();
        if($conn->connect()) {

            $this->dList = new destination_list();

            if (isset($_GET["action"])){
                $action = $_GET["action"];

                if ($action === "getAll"){
                    $this->message = (json_encode($this->dList->getAll()));
                }
                if ($action === "getPages") {
                    $this->message = (json_encode($this->dList->getPages()));
                }
                if ($action === "getDimPages") {
                    $this->message = (json_encode(destination_list::dimPagina() ));
                }
            }
        }
    }

    /**
     * @return string The current message
     */
    public function mess()
    {
        return $this->message;
    }

}


$dest_list = new dest_list();

echo $dest_list->mess();
