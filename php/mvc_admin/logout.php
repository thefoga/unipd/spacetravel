<?php

require_once (__DIR__."/model/session.php");

$session = new session();
$session->createSession();

$session->logOut();

header('Location: login.php?info=logout');
exit();

