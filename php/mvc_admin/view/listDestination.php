<?php

require_once __DIR__ . '/layoutBase.php';

/**
 * Creates HTML to show destination page
 */
class listDestination
{

    /**
     * @param $destinazioni list of destinations to show
     * @return string HTML page with destinations
     */
    private static function listArray($destinazioni){
        $html ="";

        foreach ($destinazioni as $dest ){
            $name = $dest->getTitle();
            $id = $dest->getId();
            $html .= "<div class=\"destination\" ><h3>".$name ."</h3>";
            $html .=  "<a title=\"Edit ".$name."\" href=\"edit.php?action=edit&amp;num=".$id."\">Edit<span class=\"element-invisible\">".$name."</span></a>";
            $html .=  "<a title=\"Delete ".$name."\" href=\"edit.php?action=delete&amp;num=".$id."\">Delete<span class=\"element-invisible\">".$name."</span></a>";
            $html .=  "<a title=\"Download ".$name." subscriptions\" href=\"./controller/download.php?num=".$id."\">Download <span class=\"element-invisible\">".$name."</span></a>";
            $html .=  "<a title=\"View ".$name."\" href=\"../../dest".$id.".html\">View <span class=\"element-invisible\">".$name."</span></a>";

            $html .= "</div>";
        }
        return $html;
    }

    /**
     * @param $nPagina number of page to build
     * @param $pagine total number of pages
     * @param $searched query
     * @return string HTML to display destinations list
     */
    private static function buildNavigationPages($nPagina,$pagine,$searched){
        $html = "";
        $extraInput ="";
        $gotopage = "list.php?";
        if($searched != "") {
            if(nPagina > 1)
            $gotopage = "search.php?searchtext=" . $searched . "";


            $extraInput = "<input type='hidden' value='".urldecode($searched)."' name='searchtext' />";
        }

        if($nPagina > 1)
            $html .= "<a class=\"link_dest_action nav_dest\"  href=\"".$gotopage."page=".($nPagina-1)." \">&lt; Go to previous page</a>";
        else
            $html .= "<span class=\"link_dest_action nav_dest\">&lt; Go to previous page</span>";

        if($nPagina < $pagine)
            $html .= "<a class=\"link_dest_action nav_dest\" href=\"".$gotopage."page=".($nPagina+1)."\">Go to next page &gt;</a>";
        else
            $html .= "<span class=\"link_dest_action nav_dest\">Go to next page &gt;</span>";

        $html .="<p class=\"link_dest_action nav_dest\" >Page ".$nPagina." of ".$pagine."</p>
<form class=\"link_dest_action nav_dest\" action='".$gotopage."' method='get'><fieldset>
<legend>Go to one of the destinations page</legend><label for=\"page\">Go to page</label><input id=\"page\" name=\"page\" type=\"text\"/>".$extraInput."<input value='Go' type=\"submit\"/>
</fieldset></form>";

        return $html;
    }


    /**
     * @param $dest destination to display
     * @param $nPagina number of current page
     * @param $pagine total number of pages
     * @param string $searched query
     * @return string HTML with destinations list based on query
     */
    public static function show($dest,$nPagina,$pagine,$searched = ""){

        $destH = self::listArray($dest);

        $menuH = self::buildNavigationPages($nPagina,$pagine,$searched);

        $form = layoutBase::searchBar($searched);

        $html = <<<EOD

          {$form}
   
   <a id="add_dest" class="link_dest_action" href="edit.php?action=create">Add new destination</a>

    <div class="element-invisible">
        <h2>Page layout info</h2>
        <p>You'll find Headers level 3 with the name of the planet and then 4 links for 
        that specific planet to take some actions such as editing it, deleteing, viewing or downloading user
        email subscriptions</p>   
     </div>   
        
    <div id="list_destinations">
        
    {$destH}

    </div>
    <!-- end list_destinations -->

    {$menuH}


<!-- end content -->

EOD;
        return $html;

    }

}


