<?php

/**
 * Creates HTML used for logins
 */
class loginUser
{

    /**
     * @return string HTML form to login user
     */
    public static function show()
    {

        $html = <<<EOD
    
        <form class="link_dest_action a_form" id="login_form" method="post" action="login.php">
            <fieldset>
                <legend>Curator login</legend>
                <p>
                    <label for="username">Username:</label>
                    <br/>
                    <input id="username" name="username" type="text"/>
                </p>
                <p>
                    <label for="password">Password:</label>
                    <br/>
                    <input id="password" name="password" type="password"/>
                </p>
                <span class="button"><input value="Login" type="submit"/></span>
            </fieldset>
        </form>
    

EOD;
        return $html;
    }

}