<?php

/**
 * Creates HTML with message
 */
class showMessage
{

    /**
     * @param $message string to send
     * @param string $title title of message
     * @param $alertFactor 0 -> info, 1 -> alert, 2 -> warning
     * @return string HTML code to show client
     */
    public static function show($message, $title = "", $alertFactor = 1)
    {

        $html = <<<EOD

        <div class="messageClass messageClass{$alertFactor}">

            <h2> {$title} </h2>
            
            <p>
            
                {$message}
            
            </p>

        </div>
EOD;
        return $html;

    }

}


