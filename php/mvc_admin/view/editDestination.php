<?php

/**
 * Creates raw HTML to display destination edit form
 */
class editDestination
{

    /**
     * @param $dest destination object
     * @return string raw HTML with destination edit form
     */
    public static function show($dest){

        $html = <<<EOD


<form class="a_form" id ="editor_form" action = "edit.php" method = "post" >
        <fieldset >
<input name="num" type="hidden" value="{$dest->getId()}" />
<input name="action" type="hidden" value="modify" />

<p id="form_errors" role="alert" aria-atomic="true" class="messageClass messageClass2">

</p>

<legend>Space Destination specifics</legend>
<p>
    <label for="planet_title">Name of the planet:</label>
    <br/>
    <input id="planet_title" name="planet" type="text" value="{$dest->getTitle()}" />

</p>
<p>
    <label for="planet_imageurl">Remote url of decorative image of the planet:</label>
    <br/>
    <input id="planet_imageurl" name="imageurl" type="text" value="{$dest->getImgUrl()}"/>
</p>
<p>
    <label for="planet_description">Description of the planet:</label>
    <br/>
    <textarea name="planet_description" id="planet_description" rows="20" cols="20">{$dest->getDescription()}</textarea>
    <br/>
</p>

<span class="button"><input value="Save Destination" type="submit"/></span>
</fieldset>
</form>

EOD;
        return $html;

    }

}


