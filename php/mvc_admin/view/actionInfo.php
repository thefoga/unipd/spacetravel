<?php

require_once (__DIR__ . '/showMessage.php');

/**
 * Simple class to show message about action just performed
 */
class actionInfo
{

    /**
     * @param $tipo string with type of action
     * @return string extensive explanation of action just performed
     */
    public static function show($tipo){
        $mess = "";
        switch ($tipo){
            case "loggedIn":
                $mess = showMessage::show("Login successful","Login complete",0);
                break;
            case "needLogin":
                $mess = showMessage::show("You need to login before accessing this pages","Login request Curator",2);
                break;
            case "logout":
                $mess = showMessage::show("You have been logout correctly","Logout complete",0);
                break;
            case "deleted":
                $mess = showMessage::show("The destination has been deleted","Destination deleted",1);
                break;
            case "inserted":
                $mess = showMessage::show("The destination has been inserted","Destination inserted",0);
                break;
            case "alreadyLoggedIn":
                $mess = showMessage::show("You already logged in the system","You are logged in",1);
                break;
            case "saved":
                $mess = showMessage::show("You just saved that destination!","Destination update saved",0);
                break;
            case "modify_something":
                $mess = showMessage::show("You need to modify the destination before saving!","No changes",1);
                break;
            case "fixContent":
                $mess = showMessage::show("You need to set the right value of the destination fields","Not saved",2);
                break;
        }
        return $mess;

    }

}